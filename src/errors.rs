//! Declare an error type

use thiserror::Error;

/// An error originated by a command.
#[derive(Error, Debug)]
pub enum Error {
    #[error("No more DirMgr events.")]
    NoDirMgrEvents,
}
