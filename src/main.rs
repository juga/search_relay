#![warn(unused_extern_crates)]
mod errors;
mod search_relay;

use anyhow::Result;
use futures::StreamExt;
use std::sync::Arc;
use std::time::Duration;
use structopt::StructOpt;
use tokio::time::timeout;
use tokio_crate as tokio;
use tracing::debug;
use tracing_subscriber::{filter, prelude::*};

use arti_client::{config::TorClientConfig, TorClient};
use tor_dirmgr::{DirProvider, Timeliness};
use tor_netdir::{MdReceiver, NetDir};

#[derive(StructOpt)]
pub struct Opts {
    #[structopt(short = "l", long, default_value = "main:debug")]
    pub log_level: String,
    #[structopt(
        short = "f",
        long,
        default_value = "1A25C6358DB91342AA51720A5038B72742732498"
    )]
    pub fp: String,
}

/// Listen to directory info changes until there are not missing
/// microdescriptors or timeout returning an error.
async fn get_complete_netdir(dirmgr: &Arc<dyn DirProvider>) -> Result<Arc<NetDir>> {
    let mut events = dirmgr.events();
    // Loop to wake up when the directory info changes.
    while let Some(event) = events.next().await {
        debug!("A directory info event happened: {:?}", event);
        let and = dirmgr.netdir(Timeliness::Unchecked)?;
        if and.n_missing() == 0 {
            debug!("Missing 0 microdescriptors.");
            return Ok(and);
        }
        debug!("Missing {} microdescriptors.", and.n_missing());
    }
    Err(errors::Error::NoDirMgrEvents.into())
}

async fn run(dirmgr: &Arc<dyn DirProvider>, fp: String) -> Result<()> {
    // let dirmgr = arti_client.dirmgr();
    // Duration after which waiting for a complete netdir will timeout.
    let duration = Duration::from_millis(30000);
    // Insead of waiting until all the microdescriptors arrive, it is
    // possible to search for the ones in the `filters` (see arti cede3aca)
    // but it requires to parse the filter(s) first.
    if let Ok(netdir) = timeout(duration, get_complete_netdir(dirmgr)).await? {
        debug!(
            "Number of relays: {}",
            netdir.relays().collect::<Vec<_>>().len()
        );
    } else {
    }
    let netdir = dirmgr.timely_netdir()?;
    debug!("nedir lifetime {:?}", netdir.lifetime());
    let _r = search_relay::search_relay_filter(&netdir, &fp);
    let _r = search_relay::search_relay_byid(&netdir, &fp);
    let _r = search_relay::search_relay_byrsaunchecked(&netdir, &fp);
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let opts = Opts::from_args();
    // Set logging level from cli option
    let filter = opts.log_level.parse::<filter::Targets>()?;
    // Build a new subscriber with the `fmt` layer using the `Targets`
    // filter constructed above.
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
    let fp = opts.fp;
    let config = TorClientConfig::default();
    let arti_client = TorClient::create_bootstrapped(config).await?;
    let dirmgr = arti_client.dirmgr();
    run(dirmgr, fp).await
}
