// mod errors;

use anyhow::Result;
use std::str::FromStr;
use tracing::{debug, info, warn};

use tor_linkspec::RelayId;
use tor_llcrypto::pk::rsa::RsaIdentity;
use tor_netdir::{NetDir, Relay};

pub fn relay_info(relay: &Relay) {
    info!("Found relay {}.", relay.rsa_id());
    // debug!("{:?}", relay.md());
    // let rs = relay.rs();
    // debug!("{:?}", rs);
    // let mddigest = rs.md_digest();
    // debug!("mddigest {:?}", mddigest);
}

pub fn search_relay_filter(netdir: &NetDir, fp: &String) -> Result<()> {
    let relays: Vec<Relay<'_>> = netdir
        .relays()
        .filter(|r| r.rsa_id().to_string().replace('$', "").to_uppercase() == *fp)
        .collect();
    debug!("len relays {}", relays.len());
    if relays.len() == 1 {
        let relay = relays[0].clone();
        relay_info(&relay);
    } else {
        warn!("Couldn't find relay {}.", fp);
    }
    Ok(())
}

pub fn search_relay_byid(netdir: &NetDir, fp: &String) -> Result<()> {
    let relayid = RelayId::from_str(&fp)?; // _or(errors::WrongRelayId)?;
    let relay = netdir.by_id(&relayid);
    if relay.is_some() {
        relay_info(&relay.unwrap());
    } else {
        warn!("Couldn't find relay {}.", fp);
    }
    Ok(())
}

pub fn search_relay_byrsaunchecked(netdir: &NetDir, fp: &String) -> Result<()> {
    let relayid = RelayId::from_str(&fp)?; // .ok_or(errors::WrongRelayId)?;
    let rsaid = RsaIdentity::from_bytes(relayid.as_bytes());
    if rsaid.is_some() {
        let relay = netdir.by_rsa_id_unchecked(&rsaid.unwrap());
        if relay.is_some() {
            let relay = relay.unwrap().into_relay();
            if relay.is_some() {
                relay_info(&relay.unwrap());
                return Ok(());
            }
        }
    }
    warn!("Couldn't find relay {}.", fp);
    Ok(())
}
